package com.clubmetics;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Userlogin extends AppCompatActivity {
    TextView user_signup_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlogin);
        user_signup_id=(TextView)findViewById(R.id.user_sign_up_text_id);
        user_signup_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent user_signup_text = new Intent(getApplicationContext(), SignupForUser.class);
                startActivity(user_signup_text);
            }
        });
    }
}
