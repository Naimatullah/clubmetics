package com.clubmetics;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class Admin_Registration_Form extends AppCompatActivity {
    EditText admin_full_name,admin_mobile_no,admin_email,admin_password;
    CircleImageView admin_profile_registration;
    Button admin_registration_button;
    private ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase_reference;
    private StorageReference mStorage_reference;
    HashMap<String,String> users_store_data_object=new HashMap<>();
    private static final int request_code = 1;
    private String download_url;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        mDatabase_reference = FirebaseDatabase.getInstance().getReference().child("Registered_Admin");
        mStorage_reference = FirebaseStorage.getInstance().getReference().child("Admin_images");
        progressDialog=new ProgressDialog(this);
        imageUri=null;
        setContentView(R.layout.activity_admin__registration__form);
        admin_full_name = findViewById(R.id.admin_full_name_signup_id);
        admin_mobile_no = findViewById(R.id.admin_mobile_number_sign_up_id);
        admin_email = findViewById(R.id.admin_email_address_sign_up_id);
        admin_password = findViewById(R.id.admin_password_signup_id);
        admin_profile_registration = findViewById(R.id.admin_profile_registration_id);
        admin_registration_button = findViewById(R.id.admin_registration_btn_id);
        admin_registration_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registeradmin(admin_email.getText().toString(),admin_password.getText().toString());
            }
        });
        admin_profile_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");
                Intent pickIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                startActivityForResult(chooserIntent, request_code);
            }
        });


    }
    private void registeradmin(String email, String password) {
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("While Creating Your Account");
        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String status = "1";
                    users_store_data_object.put("Profile", download_url);
                    users_store_data_object.put("FullName", admin_full_name.getText().toString());
                    users_store_data_object.put("ContactNo", admin_mobile_no.getText().toString());
                    users_store_data_object.put("Email", admin_email.getText().toString());
                    users_store_data_object.put("Password", admin_password.getText().toString());
                    users_store_data_object.put("Status",status.toString());
                    //users.put("FCM_Token",token);
                    mDatabase_reference.child(mAuth.getCurrentUser().getUid()).push().setValue(users_store_data_object).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                progressDialog.dismiss();
                                Intent intent = new Intent(Admin_Registration_Form.this, Userlogin.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                //SaveClientsDetails();
                                startActivity(intent);
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(Admin_Registration_Form.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(Admin_Registration_Form.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCode && resultCode == RESULT_OK) {
            imageUri = data.getData();
            String image_id = UUID.randomUUID().toString();
            admin_profile_registration.setImageURI(imageUri);
            StorageReference user_profile = mStorage_reference.child(image_id + ".jpg");
            user_profile.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (taskSnapshot.getMetadata() != null) {
                        if (taskSnapshot.getMetadata().getReference() != null) {
                            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                            result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    download_url = uri.toString();
                                    users_store_data_object.put("Profile", download_url);
                                }
                            });
                        } else {
                            users_store_data_object.put("Profile", "");
                        }
                    }
                }

            });
        }
    }
}
