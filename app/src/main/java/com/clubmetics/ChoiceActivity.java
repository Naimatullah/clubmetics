package com.clubmetics;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChoiceActivity extends AppCompatActivity {
    private Button mUser_login_btn,magent_login_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);
        mUser_login_btn = (Button)findViewById(R.id.user_login_btn_id);
        mUser_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mUserlogin_intent=new Intent(getApplicationContext(), Userlogin.class);
                startActivity(mUserlogin_intent);
            }
        });
       magent_login_btn =(Button)findViewById(R.id.agentbtn);
       magent_login_btn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent magentlogin_intent = new Intent(getApplicationContext(),Userlogin.class);
               startActivity(magentlogin_intent);
           }
       });


    }
}
