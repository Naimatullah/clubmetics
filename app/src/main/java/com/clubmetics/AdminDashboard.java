package com.clubmetics;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import es.dmoral.toasty.Toasty;

public class AdminDashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ActionBarDrawerToggle toggle;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private String user_current_id, messages;
    public String ClientImageslink;
    private NavigationView mNavigationView;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase_reference;
    private FirebaseUser firebaseUser;
    private ProgressDialog progressBar;
    private TextView mName, mUsername;
    private String profile_link;
    private ImageView mUser_profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);

    }    private void Init_Navigation() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_admin_id);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.close, R.string.open);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerLayout.requestLayout();
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setTitle("");
        progressBar = new ProgressDialog(this);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_admin_menu_id);
        mNavigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);// you need to set Listener.
        final View headerView = LayoutInflater.from(this).inflate(R.layout.admin_navigation_drawer_layout, mNavigationView, false);
        //mNavigationView.getMenu().getItem(1).setActionView(R.layout.image_case_more);
        mNavigationView.addHeaderView(headerView);
        mName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.admin_navi_name_id);
        mUsername = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.admin_navi_email_id);
        mUser_profile = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.user_navigation_profile);

        User_Authentication();
        try {
            mDatabase_reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    AdminModel adminModel_object = new AdminModel();
                    adminModel_object.setmAdmin_name(dataSnapshot.child("FullName").getValue(String.class));
                    adminModel_object.setmAdmin_email(dataSnapshot.child("Email").getValue(String.class));
                    String name = adminModel_object.getmAdmin_name();
                    String email = adminModel_object.getmAdmin_email();
                    mName.setText(name);
                    mUsername.setText(email);
                    try {
                        profile_link = dataSnapshot.child("Profile").getValue(String.class);
                        Glide.with(getApplicationContext()).load(profile_link).into(mUser_profile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
            toggle.syncState();
        } catch (Exception e) {
            e.printStackTrace();
        }
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        toggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        NavigationView mNavigationView = (NavigationView) findViewById(R.id.navigation_admin_menu_id);
        int id = menuItem.getItemId();
        Menu m = mNavigationView.getMenu();
        if (id == R.id.admin_alert_id) {
          /*  Intent profile_intent = new Intent(getApplicationContext(), My_Profile.class);
            startActivity(profile_intent);*/
       /* }else if(id==R.id.item_location_id) {
            Intent profile_intent = new Intent(getApplicationContext(), Map_Tracking_Screen.class);
            startActivity(profile_intent);
        }
        else if(id==R.id.item_about_us_id) {
            Intent about_intent = new Intent(getApplicationContext(), About_us.class);
            startActivity(about_intent);
        }
        else if(id==R.id.item_contact_us_id) {
            Intent contact_intent = new Intent(getApplicationContext(), Contact_us.class);
            startActivity(contact_intent);
        }
        else if(id==R.id.item_help_id) {
            Toasty.info(getApplicationContext(),"Coming Soon !",Toasty.LENGTH_LONG).show();
        }
        else if(id==R.id.item_notifications_id) {
            Toasty.info(getApplicationContext(),"No notification received !",Toasty.LENGTH_LONG).show();
        }*/
        }
        else if ( id== R.id.admin_setting_id) {
            Toasty.success(getApplicationContext(), "Successfully Sign Out !", Toasty.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(),"Successfully Sign Out !",Toast.LENGTH_LONG).show();
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(getApplicationContext(), Userlogin.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);            /*Intent i = new Intent(getApplicationContext(), Appointment_Schedule.class);
            startActivity(i);*/
        }
        return true;
    }

    private void User_Authentication() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        mDatabase_reference = FirebaseDatabase.getInstance().getReference().child("AdminRegister").child(firebaseUser.getUid());
    }
}
