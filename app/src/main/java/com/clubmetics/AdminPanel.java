package com.clubmetics;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdminPanel extends AppCompatActivity {
private EditText product_heading,product_description,product_price;
private CircleImageView product_image;
private Button add_product_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__panel_);
        product_heading=findViewById(R.id.admin_add_heading);
        product_description=findViewById(R.id.admin_add_discription);
        product_price =findViewById(R.id.admin_add_price);
        add_product_button = findViewById(R.id.admin_add_data_button);
        product_image= findViewById(R.id.admin_add_product_profile);

    }
}
