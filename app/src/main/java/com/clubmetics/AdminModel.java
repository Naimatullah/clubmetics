package com.clubmetics;

public class AdminModel {
    private String mAdmin_name;
    private String mAdmin_email;
    private String mAdmin_password;
    private String mContact_no;
    private String mAdmin_profile;
    public String getmAdmin_name() {
        return mAdmin_name;
    }

    public void setmAdmin_name(String mAdmin_name) {
        this.mAdmin_name = mAdmin_name;
    }

    public String getmAdmin_email() {
        return mAdmin_email;
    }

    public void setmAdmin_email(String mAdmin_email) {
        this.mAdmin_email = mAdmin_email;
    }

    public String getmAdmin_password() {
        return mAdmin_password;
    }

    public void setmAdmin_password(String mAdmin_password) {
        this.mAdmin_password = mAdmin_password;
    }

    public String getmContact_no() {
        return mContact_no;
    }

    public void setmContact_no(String mContact_no) {
        this.mContact_no = mContact_no;
    }

    public String getmAdmin_profile() {
        return mAdmin_profile;
    }

    public void setmAdmin_profile(String mAdmin_profile) {
        this.mAdmin_profile = mAdmin_profile;
    }

}
