package com.clubmetics;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DentalDataAdapter extends RecyclerView.Adapter<DentalDataAdapter.viewHolder> {
    Context context;
    ArrayList <AllProductModel> all_product_list;



    public DentalDataAdapter(Context context, ArrayList<AllProductModel>all_product_list) {
        this.context = context;
        this.all_product_list= all_product_list;

    }

    @Override
    public  DentalDataAdapter.viewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.show_dental_data, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public  void onBindViewHolder(DentalDataAdapter.viewHolder viewHolder, int position) {
        viewHolder.heading.setText(all_product_list.get(position).getHeading());
        viewHolder.description.setText(all_product_list.get(position).getDescription());
        viewHolder.price.setText(all_product_list.get(position).getPrice());
        viewHolder.icon.setImageResource(all_product_list.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return all_product_list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        CircleImageView icon;
        TextView heading;
        TextView description;
        TextView price;

        public viewHolder(View itemView) {
            super(itemView);
            icon = (CircleImageView) itemView.findViewById(R.id.profile);
            heading = (TextView) itemView.findViewById(R.id.denal_heading_id);
            description = (TextView)itemView.findViewById(R.id.dental_desccription_id);
            price=(TextView)itemView.findViewById(R.id.dental_price_id);
        }
    }

}
